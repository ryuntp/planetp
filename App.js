/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import Camera from "./src/components/Camera"
import React, { Component } from "react"
import {
	Platform,
	StyleSheet,
	Text,
	View,
	TouchableOpacity
} from "react-native"
import Router from "./src/RouterComponent"
import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { Provider } from "react-redux"
import RootReducer from "./src/store/reducers/rootReducer"

import SplashScreen from "react-native-splash-screen"
console.disableYellowBox = true
const store = createStore(RootReducer, {}, applyMiddleware(thunk))

const styles = {
	container: {
		flex: 1,
		justifyContent: "center",
		backgroundColor: "#F5FCFF",
		height: 60
	},
	welcome: {
		fontSize: 20,
		textAlign: "center",
		margin: 10
	},
	instructions: {
		textAlign: "center",
		color: "#333333",
		marginBottom: 5
	}
}

export default class App extends Component {
	state = {
		clicked: false
	}

	handleChangeState = () => {
		this.setState(prevState => {
			return { clicked: !prevState.clicked }
		})
	}
	componentDidMount() {
		SplashScreen.hide()
	}
	render() {
		return (
			<Provider store={store}>
				<View style={styles.container}>
					<Router />
				</View>
			</Provider>
		)
	}
}
