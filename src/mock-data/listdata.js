export const listdata = [
	{
		id: 1,
		image: "https://via.placeholder.com/150/000000/FFFFFF/?text=MockImg",
		title: "Chiang Mai University",
		datetime: "10/01/1996",
		detail: "Mockdata is data was made to show only not use",
		location: "Chiang Mai",
		geolocation: {
			latitude: 18.8043949,
			longitude: 98.9526183
		}
	},
	{
		id: 2,
		image: "https://via.placeholder.com/150/000000/FFFFFF/?text=MockImg",
		title: "Chiang Mai International Airport (CNX)",
		datetime: "10/01/1996",
		detail: "Mockdata is data was made to show only not use",
		location: "Chiang Mai",
		geolocation: {
			latitude: 18.767749,
			longitude: 98.9618148
		}
	},
	{
		id: 3,
		image: "https://via.placeholder.com/150/000000/FFFFFF/?text=MockImg",
		title: "Bangkok",
		datetime: "10/01/1996",
		detail: "Mockdata is data was made to show only not use",
		location: "Bangkok",
		geolocation: {
			latitude: 13.7245599,
			longitude: 100.4926816
		}
	},
	{
		id: 4,
		image: "https://via.placeholder.com/150/000000/FFFFFF/?text=MockImg",
		title: "Biosci(Thailand)Co.,Ltd",
		datetime: "10/01/1996",
		detail: "Mockdata is data was made to show only not use",
		location: "Pathumthani",
		geolocation: {
			latitude: 14.076996,
			longitude: 100.600785
		}
	}
];
