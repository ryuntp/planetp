export const navBarStyles = {
	container: {
		flex: 1
	},
	gradientLayout: {
		width: "100%",
		height: "100%",
		flexDirection: "row",
		justifyContent: "center"
	},
	viewStyle: {
		height: 60,
		shadowOffset: { width: 0, height: 2 },
		shadowColor: "#000",
		shadowOpacity: 0.9,
		elevation: 10,
		position: "relative",
		flexDirection: "column"
	},
	menusStyle: {
		flexDirection: "row"
	},
	menuButton: {
		paddingLeft: 15,
		paddingRight: 15,
		flexDirection: "column",
		justifyContent: "center"
	},
	menuText: {
		fontSize: 18,
		color: "#FFFFFF"
	},
	textStyle: {
		fontSize: 20,
		color: "#FFFFFF",
		alignSelf: "center"
	},
	actIn: {
		flex: 1,
		justifyContent: "space-between"
	},
	feedicon: {
		alignSelf: "center",
		width: 26,
		height: 26,
		position: "relative",
		marginLeft: 14,
		marginRight: 14
	},
	cameraicon: {
		alignSelf: "center",
		width: 31,
		height: 31,
		position: "relative",
		marginLeft: 14,
		marginRight: 14
	},
	mapicon: {
		alignSelf: "center",
		width: 26,
		height: 26,
		position: "relative",
		marginLeft: 14,
		marginRight: 14
	},
	formsubicon: {
		alignSelf: "center",
		width: 26,
		height: 26,
		position: "relative",
		marginLeft: 14,
		marginRight: 14
	},
	logoicon: {
		alignSelf: "center",
		width: 120,
		height: 50,
		position: "relative"
	}
}
