export const cardDetailStyles = {
	headerContentStyle: {
		flexDirection: "column",
		justifyContent: "space-around"
	},
	headerTextStyle: {
		fontSize: 18
	},
	thumbnailStyle: {
		height: 50,
		width: 50,
		justifyContent: "space-around"
	},
	thumbnailContainerStyle: {
		justifyContent: "center",
		alignItems: "center",
		marginLeft: 10,
		marginRight: 10
	},
	imageStyle: {
		height: 300,
		flex: 1,
		borderRadius: 10,
		width: null
	},
	textStyle: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		fontSize: 16
	}
}
