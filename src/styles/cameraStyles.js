import { StyleSheet, Dimensions } from "react-native"

export const cameraStyles = StyleSheet.create({
	container: {
		flex: 1
	},
	preview: {
		flex: 1,
		justifyContent: "flex-end",
		alignItems: "center"
	},
	imgPreview: {
		height: Dimensions.get("window").height,
		width: Dimensions.get("window").width
	},
	capture: {
		flex: 0,
		backgroundColor: "#15B8C4",
		borderRadius: 50,
		padding: 5,
		alignSelf: "center",
		margin: 15
	},
	pending: {
		flex: 1,
		backgroundColor: "lightgreen",
		justifyContent: "center",
		alignItems: "center"
	},
	cameraSurface: {
		flex: 0,
		flexDirection: "row",
		justifyContent: "center"
	},
	cameraMenu: {
		position: "absolute",
		bottom: 5,
		flexDirection: "row",
		justifyContent: "center",
		alignSelf: "center"
	},
	cameraMenuFont: {
		fontSize: 15,
		color: "#FFFFFF",
		textAlign: "center",
		margin: 15
	},
	loading: {
		flex: 1,
		zIndex: 3,
		alignItems: "center"
	}
})
