import { StyleSheet } from "react-native"

export const FormSubmissionStyles = StyleSheet.create({
	loading: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center"
	},
	alertBox: {
		backgroundColor: "#1C97F7"
	},
	alertText: {
		fontSize: 12,
		color: "#ffffff"
	},
	conCard: {
		marginLeft: 25,
		marginRight: 25,
		marginTop: 20
	},
	conCardItem: {
		marginLeft: 5,
		marginTop: 5
	},
	conDetails: {
		fontSize: 15,
		color: "black",
		marginLeft: 5
	},
	postCard: {
		marginLeft: 25,
		marginRight: 25,
		marginTop: 20,
		marginBottom: 20,
		alignItems: "center"
	},
	button: {
		alignItems: "center",
		padding: 10,
		borderRadius: 10,
		alignSelf: "center"
	},
	Returnbutton: {
		alignItems: "center",
		padding: 10,
		borderRadius: 10,
		alignSelf: "center"
	},
	buttonText: {
		fontSize: 16
	},
	HeaderText: {
		fontSize: 25
	},
	LinearGradientLogin: {
		height: "100%",
		width: "100%"
	},
	infotxt: {
		fontSize: 18,
		color: "gray"
	},
	container: {
		flex: 1
	},
	submitButton: {
		width: 200,
		backgroundColor: "#74C66D",
		borderRadius: 30,
		padding: 5,
		alignSelf: "center",
		marginTop: 20,
		marginBottom: 20
	},
	submitButtonFont: {
		fontSize: 15,
		color: "#FFFFFF",
		textAlign: "center",
		margin: 10
	}
})
