export const healthyBarStyle = (inputRandomNumber) => {
    return {
        nullTube: {
            position: "absolute",
            left: inputRandomNumber,
            borderWidth: 2,
            borderColor: "white",
            height: 20,
            width: 5,
            zIndex: 1,
            backgroundColor: "#000000"
        },
        indicator: {
            height: 20,
            width: 175,
            zIndex: 0
        }
    }
}