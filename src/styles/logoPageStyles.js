export const logoPageStyle = {
	container: {
		height: "100%",
		width: "100%"
	},
	menuFloat: {
		flex: 1,
		height: "100%",
		width: "100%",
		position: "absolute",
		alignContent: "center",
		padding: 0
	},
	textInputContainer: {
		width: "80%",
		height: 45,
		alignSelf: "center",
		padding: 5,
		borderRadius: 5,
		backgroundColor: "white",
		margin: 10
	},
	loginButtonFont: {
		fontSize: 15,
		color: "#FFFFFF",
		textAlign: "center",
		margin: 10
	},
	imageStyle: {
		alignSelf: "center",
		width: 220,
		height: 100,
		marginBottom: 50,
		marginTop: 60,
		padding: 0,
		position: "relative"
	},
	imageSignupStyle: {
		alignSelf: "center",
		width: 220,
		height: 100,
		marginBottom: 20,
		marginTop: 30,
		padding: 0,
		position: "relative"
	},
	loginButton: {
		flex: 0,
		backgroundColor: "#74C66D",
		borderRadius: 5,
		padding: 5,
		paddingLeft: 96,
		paddingRight: 96,
		alignSelf: "center",
		marginTop: 20,
		marginBottom: 40
	},
	loginSignupButton: {
		width: "80%",
		backgroundColor: "#74C66D",
		borderRadius: 5,
		padding: 5,
		paddingLeft: 96,
		paddingRight: 96,
		alignSelf: "center",
		marginTop: 20,
		marginBottom: 0
	},
	btnsignup: {
		color: "white",
		textDecorationLine: "underline",
		fontSize: 16,
		alignSelf: "center",
		marginBottom: 50
	},
	containerlogin: {
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "column",
		flex: 1,
		margin: 20,
		padding: 0,
		backgroundColor: "rgba(0,0,0,0.3)",
		borderRadius: 10
	}
}
