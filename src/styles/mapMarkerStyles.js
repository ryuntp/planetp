export const mapMarkerStyles = {
    markerContainer: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center"
    },
    pinImage: {
        width: 50,
        height: 50,
        resizeMode: "cover",
        borderRadius: 10,
        borderWidth: 2,
        borderColor: "#15B8C4"
    },
    captionTitle: {
        margin: 1,
        padding: 1,
        borderRadius: 10,
        borderColor: "#15B8C4",
        backgroundColor: "#FFF"
    }
}