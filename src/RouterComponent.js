import React, { Component } from "react"
import { Scene, Router, Tabs, Stack } from "react-native-router-flux"

import LogoPage from "./components/Logopage"
import CardList from "./components/CardList"
import Camera from "./components/Camera"
import Map from "./components/Map"
import Detail from "./components/Detail"
import SignUp from "./components/SignUp"
import FormSub from "./components/FormSub"

class RouterComponent extends Component {
	render() {
		return (
			<Router>
				<Stack key="root">
					<Scene key="LogoPage" component={LogoPage} hideNavBar={true} />
					<Scene key="CardList" component={CardList} hideNavBar={true} />
					<Scene key="Camera" component={Camera} hideNavBar={true} />
					<Scene key="Map" component={Map} hideNavBar={true} />
					<Scene key="Detail" component={Detail} hideNavBar={true} />
					<Scene key="SignUp" component={SignUp} hideNavBar={true} />
					<Scene key="FormSub" component={FormSub} hideNavBar={true} />
				</Stack>
			</Router>
		)
	}
}

export default RouterComponent
