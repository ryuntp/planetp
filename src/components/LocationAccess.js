import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

const defaultFontStyle = { fontSize: 14 };

class LocationAccess extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: null,
            longitude: null
        }
    }

    

    findCurrentLocation = () => {
        // FIXME: This API is the low accuracy (Google told that!)
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("Position:", position);
                const latitude = JSON.stringify(position.coords.latitude);
                const longitude = JSON.stringify(position.coords.longitude);
                this.setState({ 
                    latitude: latitude, 
                    longitude: longitude
                });
            }, 
            (error) => console.log(error), {
                timeout: 20000,
                maximumAge: 1000,
                enableHighAccuracy: false
            });
    }

    render() {
        return (
            <TouchableOpacity onPress={this.findCurrentLocation}>
                <Text style={defaultFontStyle}>Where am I?</Text>
                <Text style={defaultFontStyle}>{this.state.latitude}</Text>
                <Text style={defaultFontStyle}>{this.state.longitude}</Text>
            </TouchableOpacity>
        );
    }
}

export default LocationAccess;