import React, { Component } from "react"
import logo from "../../assets/planet_p_logobar.png"
import LinearGradient from "react-native-linear-gradient"
import {
	View,
	ScrollView,
	Text,
	TextInput,
	Image,
	TouchableOpacity,
	ToastAndroid
} from "react-native"
import { Actions } from "react-native-router-flux"
import { logoPageStyle } from "../styles/logoPageStyles"
import Toast from "react-native-tiny-toast"
import AsyncStorage from "@react-native-community/async-storage"
import { ipaddr, port } from "../configs/configuration"
import SplashScreen from "react-native-splash-screen"
let seenlogin = []
class LogoPage extends Component {
	constructor(props) {
		super(props)
		this.state = {
			email: "",
			password: ""
		}
	}
	componentDidMount() {
		// do stuff while splash screen is shown
		// After having done stuff (such as async tasks) hide the splash screen
		SplashScreen.hide()
	}

	render() {
		const {
			container,
			imageStyle,
			textInputContainer,
			loginButton,
			loginButtonFont,
			menuFloat,
			btnsignup,
			containerlogin
		} = logoPageStyle
		return (
			<LinearGradient colors={["#15B8C4", "#18B691"]} style={container}>
				<ScrollView>
					<View style={containerlogin}>
						<Image style={imageStyle} source={logo} />
						<TextInput
							style={textInputContainer}
							placeholder="Email Address"
							autoCapitalize="none"
							onChangeText={email => this.setState({ email })}
							value={this.state.email.toLowerCase()}
							keyboardType={"email-address"}
						/>
						<TextInput
							style={textInputContainer}
							secureTextEntry={true}
							autoCapitalize="none"
							placeholder="Password"
							onChangeText={password => this.setState({ password })}
							value={this.state.password}
						/>
						<TouchableOpacity style={loginButton} onPress={this.handleLogin}>
							<Text style={loginButtonFont}>Sign In</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={{ alignSelf: "center" }}
							onPress={() => {
								Actions.SignUp()
							}}
						>
							<Text style={btnsignup}>Don't have an account?</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</LinearGradient>
		)
	}

	storedata = async responseData => {
		// To set JWT as an item in AsyncStorage
		try {
			await AsyncStorage.setItem("token", responseData.token)
			const TokenInStorage = await AsyncStorage.getItem("token")
			console.log("Token in storage => ", TokenInStorage)
		} catch (e) {
			console.log("erorrrrrrrrrrrrrrrr", e)
		}
	}

	handleLogin = () => {
		const { email, password } = { ...this.state }
		if (email !== "" && password != "") {
			fetch(`${ipaddr}:${port}/login`, {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json"
				},
				body: JSON.stringify(
					{
						email: email,
						password: password
					},
					function(key, val) {
						if (val != null && typeof val == "object") {
							if (seenlogin.indexOf(val) >= 0) {
								return
							}
							console.log("mailll", email)
							console.log("passs", password)
							seenlogin.push(val)
						}
						return val
					}
				)
			})
				.then(responseData => {
					if (responseData.status === 200) return responseData.json()
				})
				.then(responseData => {
					console.log("responseData => ", responseData)
					if (responseData.token !== "") {
						this.setState({
							password: null
						})
						this.storedata(responseData)
						Toast.showSuccess("Logged in sucessfully", { duration: 800 })
						Actions.CardList()
					} else {
						ToastAndroid.show(
							"incorrect email or password!!!",
							ToastAndroid.SHORT
						)
					}
				})
				.catch(login_error => {
					console.log("login_error ===>", login_error)

					ToastAndroid.show("incorrect email or password", ToastAndroid.SHORT)
				})
		} else {
			ToastAndroid.show(
				"please fill both email and password ",
				ToastAndroid.SHORT
			)
		}
	}
}

export default LogoPage
