import React, { Component, Fragment } from "react"
import LinearGradient from "react-native-linear-gradient"
import { View, Alert, Dimensions, Image, TouchableOpacity } from "react-native"
import AwesomeButtonRick from "react-native-really-awesome-button/src/themes/rick"
import { connect } from "react-redux"
import {
	Container,
	Content,
	Text,
	Card,
	CardItem,
	Item,
	Body,
	Right,
	Input,
	Form,
	Textarea,
	Left
} from "native-base"
import { RadioButton } from "react-native-paper"
import { FormSubmissionStyles } from "../styles/FormSubmissionStyles"
import AsyncStorage from "@react-native-community/async-storage"
import { ipaddr, port } from "../configs/configuration"
import { Navbar, ButtomBar } from "./common"
const x = "          "
const {
	postCard,
	button,
	Returnbutton,
	buttonText,
	LinearGradientLogin,
	infotxt,
	container,
	submitButton,
	submitButtonFont
} = FormSubmissionStyles
class FormSubscreen extends Component {
	constructor(props) {
		super(props)
		this.state = {
			name: null,
			mobile: null,
			email: null,
			gender: null,
			message: null,
			isSubmited: false
		}
	}

	static navigationOptions = {
		header: null
	}

	postMsg = async (gender, message, msgClear) => {
		if (this.state.message != null) {
			const TokenInStorage = await AsyncStorage.getItem("token") //To get token that will be used for authorization
			await fetch(`${ipaddr}:${port}/forms`, {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					authorization: TokenInStorage
				},
				body: JSON.stringify({
					name: this.props.data[0].name,
					mobile: this.props.data[0].mobile,
					email: this.props.data[0].email,
					gender: gender,
					message: message
				})
			})
				.then(response => response.json())
				.then(responseData => {
					if (responseData != null) {
						this.refs[msgClear].setNativeProps({ text: "" })
						this.setState({
							message: null,
							gender: null,
							isSubmited: true
						})
					} else {
						Alert.alert(
							"Oops !",
							"Something went wrong",
							[
								{
									text: "OK",
									onPress: () => console.log("Cancel Pressed"),
									style: "cancel"
								}
							],
							{ cancelable: false }
						)
					}
				})
				.done()
		} else {
			Alert.alert(
				"Oops !",
				"Press SUBMIT button after entering your Message.",
				[
					{
						text: "OK",
						onPress: () => console.log("Cancel Pressed"),
						style: "cancel"
					}
				],
				{ cancelable: false }
			)
		}
	}

	_togglePostCard() {
		this.setState({
			isSubmited: false
		})
	}
	render() {
		console.log("Data naja ===>", this.props.data)

		return (
			<View style={container}>
				<Navbar title="PLANET-P" />
				<Container>
					<LinearGradient
						start={{ x: 0, y: 0 }}
						end={{ x: 1, y: 1 }}
						colors={["#15B8C4", "#18B691"]}
						style={LinearGradientLogin}
					>
						<Content>
							<Card style={postCard}>
								{this.state.isSubmited ? (
									<View>
										<CardItem>
											<Item>
												<Text style={{ flex: 1 }}>
													Your form is submitted :D
												</Text>
											</Item>
										</CardItem>
										<CardItem>
											<Left />
											<TouchableOpacity
												style={submitButton}
												onPress={() => this._togglePostCard()}
											>
												<Text style={submitButtonFont}>Return</Text>
											</TouchableOpacity>
											<Right />
										</CardItem>
									</View>
								) : (
									/////////////////////////////////////////////////////////
									<View>
										<CardItem>
											<Text style={infotxt}>
												Name: {this.props.data[0].name}
											</Text>
										</CardItem>
										<CardItem>
											<Text style={infotxt}>
												Mobile: {this.props.data[0].mobile}
											</Text>
										</CardItem>
										<CardItem>
											<Text style={infotxt}>
												Email: {this.props.data[0].email}
											</Text>
										</CardItem>
										<CardItem>
											<RadioButton.Group
												onValueChange={gender => {
													console.log("gender", gender)
													this.setState({ gender })
												}}
												value={this.state.gender}
											>
												<View>
													<Text>Male</Text>
													<RadioButton value="Male" />
												</View>
												<Text>{x}</Text>
												<View>
													<Text>Female</Text>
													<RadioButton value="Female" />
												</View>
											</RadioButton.Group>
										</CardItem>
										<Form style={{ marginLeft: 10, marginRight: 10 }}>
											<Textarea
												rowSpan={5}
												width={250}
												alignSelf={"center"}
												bordered
												placeholder="Type your message"
												onChangeText={message => this.setState({ message })}
												ref={"msgClear"}
											/>
										</Form>
										<CardItem>
											<Left />
											<TouchableOpacity
												style={submitButton}
												onPress={() =>
													this.postMsg(
														this.state.gender,
														this.state.message,
														"msgClear"
													)
												}
											>
												<Text style={submitButtonFont}>Submit</Text>
											</TouchableOpacity>
											<Right />
										</CardItem>
									</View>
								)}
							</Card>
						</Content>
					</LinearGradient>
				</Container>
				<ButtomBar />
			</View>
		)
	}
}

const mapStateToProps = state => {
	return {
		data: state.UserInfoReducer.data //initial state in reducer
	}
}

const mapDispatchToProps = {} //action
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(FormSubscreen)
