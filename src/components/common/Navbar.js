import React from "react"
import LinearGradient from "react-native-linear-gradient"
import { Text, View, Image } from "react-native"

import { navBarStyles } from "../../styles/navBarStyles"

const Navbar = props => {
	const {
		viewStyle,
		textStyle,
		menusStyle,
		gradientLayout,
		logoicon
	} = navBarStyles
	const { title } = props
	return (
		<View style={viewStyle}>
			<LinearGradient colors={["#15B8C4", "#18B691"]} style={gradientLayout}>
				<Image
					style={logoicon}
					source={require("/home/biosci/Desktop/planet-p/assets/planet_p_logobar.png")}
				/>
			</LinearGradient>
		</View>
	)
}

export { Navbar }
