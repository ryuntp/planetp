import React from "react"
import LinearGradient from "react-native-linear-gradient"
import { TouchableOpacity, Text, View, Image } from "react-native"
import { Actions } from "react-native-router-flux"
import { navBarStyles } from "../../styles/navBarStyles"

const ButtomBar = () => {
	const {
		viewStyle,
		menusStyle,
		menuButton,
		menuText,
		gradientLayout,
		feedicon,
		cameraicon,
		mapicon,
		formsubicon
	} = navBarStyles
	return (
		<View style={viewStyle}>
			<LinearGradient colors={["#15B8C4", "#18B691"]} style={gradientLayout}>
				<View style={menusStyle}>
					<TouchableOpacity
						style={menuButton}
						onPress={() => {
							Actions.CardList()
						}}
					>
						<Image
							style={feedicon}
							source={require("/home/biosci/Desktop/planet-p/assets/feed.png")}
						/>
						{/* <Text style={menuText}>Feeds</Text> */}
					</TouchableOpacity>
					<TouchableOpacity
						style={menuButton}
						onPress={() => {
							Actions.Camera()
						}}
					>
						<Image
							style={cameraicon}
							source={require("/home/biosci/Desktop/planet-p/assets/camera.png")}
						/>
						{/* <Text style={menuText}>Camera</Text> */}
					</TouchableOpacity>
					<TouchableOpacity
						style={menuButton}
						onPress={() => {
							Actions.Map()
						}}
					>
						<Image
							style={mapicon}
							source={require("/home/biosci/Desktop/planet-p/assets/map.png")}
						/>
						{/* <Text style={menuText}>Map</Text> */}
					</TouchableOpacity>
					<TouchableOpacity
						style={menuButton}
						onPress={() => {
							Actions.FormSub()
						}}
					>
						<Image
							style={formsubicon}
							source={require("/home/biosci/Desktop/planet-p/assets/formsub.png")}
						/>
						{/* <Text style={menuText}>Formsub</Text> */}
					</TouchableOpacity>
				</View>
			</LinearGradient>
		</View>
	)
}

export { ButtomBar }
