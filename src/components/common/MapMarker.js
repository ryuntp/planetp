import React, { Component } from "react"
import { Text, Image, View } from "react-native"
import { Marker } from "react-native-maps"
import { mapMarkerStyles } from "../../styles/mapMarkerStyles"

export default class MapMarker extends Component {
	constructor(props) {
		super(props)
		this.state = {
			imgUri: ""
		}
	}

	componentDidMount() {
		console.log("props in Map-marker => ", this.props)
	}

	render() {
		const { pinImage, markerContainer, captionTitle } = mapMarkerStyles
		const {
			// title,
			latitude,
			longitude,
			uri
		} = this.props
		const latint = parseFloat(latitude)
		const longint = parseFloat(longitude)

		if (latitude !== null && longitude !== null) {
			return (
				<Marker coordinate={{ latitude: latint, longitude: longint }}>
					<View style={markerContainer}>
						{uri ? <Image style={pinImage} source={{ uri }} /> : null}
						{/* <Text style={captionTitle}>{ title }</Text> */}
					</View>
				</Marker>
			)
		}
		return null
	}
}
