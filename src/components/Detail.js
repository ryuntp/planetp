import React, { Component } from "react"
import { Text, View, Image, ScrollView } from "react-native"
import DiseaseInfo from "./DiseaseInfo"
import HealthyBar from "./HealthyBar"
import { Navbar, ButtomBar, CardSection, Card } from "./common"
import { cardDetailStyles } from "../styles/cardDetailStyles"
import { MONTHS } from "../utils/constants"
import { randomInteger } from "../utils/randomInteger"

class Detail extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { lat, long, createdDate, createdTime, imageUri } = this.props
		const { headerContentStyle, imageStyle, textStyle } = cardDetailStyles
		return (
			<ScrollView>
				<Navbar title="PLANET-P" />
				<Card>
					<CardSection>
						<View style={headerContentStyle}>
							<Text
								style={headerContentStyle}
							>{`Created on: ${createdDate} | ${createdTime}`}</Text>
							{this.locationComponent(lat, long)}
						</View>
					</CardSection>
					<CardSection>
						<Image style={imageStyle} source={{ uri: imageUri }} />
					</CardSection>
					<CardSection>
						<Text style={textStyle}>
							Potential Yield: {randomInteger(30)} kg/rai
						</Text>
					</CardSection>
					<DiseaseInfo />
					<HealthyBar />
				</Card>
				<ButtomBar />
			</ScrollView>
		)
	}

	// titleComponent = (location) => {
	//     const { headerTextStyle } = cardDetailStyles;
	//     if(location) return <Text style={headerTextStyle}>{location.name}</Text>;
	//     return <Text style={headerTextStyle}>{`DCIM-remain-Images`}</Text>;
	// }

	locationComponent = (lat, long) => {
		const { headerContentStyle } = cardDetailStyles
		if (lat && long)
			return (
				<Text style={headerContentStyle}>{`Location: ${lat}, ${long}`}</Text>
			)
		return (
			<Text
				style={headerContentStyle}
			>{`Location: User does not defined`}</Text>
		)
	}
}

export default Detail
