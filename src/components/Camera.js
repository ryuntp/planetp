//problem TUESDAY : file : null , Photo is not uploaded
import React from "react"
import { RNCamera } from "react-native-camera"
import { withNavigationFocus } from "react-navigation"
import { randomInteger } from "../utils/randomInteger"
import {
	Text,
	View,
	Image,
	CameraRoll,
	TouchableOpacity,
	ToastAndroid,
	Platform,
	ActivityIndicator
} from "react-native"

import SaveLocationFile from "../utils/SaveLocationFile"
import { Navbar, ButtomBar } from "./common"
import { navBarStyles } from "../styles/navBarStyles"
import { cameraStyles } from "../styles/cameraStyles"
import {
	LOCATION_REQUEST_TIMEOUT,
	LOCATION_REQUEST_MAX_AGE
} from "../utils/constants"
import ImagePicker from "react-native-image-picker"
import { uploadModule } from "../utils/uploadImage"
import { ipaddr, port } from "../configs/configuration"
const PendingView = () => (
	<View style={cameraStyles.pending}>
		<Text>Waiting</Text>
	</View>
)

const createFormData = (photo, body) => {
	console.log("BODY TO BE SENT TO BACKEND", body)
	const data = new FormData()
	if (photo.fileName) {
		data.append("photo", {
			name: photo.fileName,
			type: photo.type,
			uri:
				Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
		})

		Object.keys(body).forEach(key => {
			data.append(key, body[key])
		})

		return data
	} else {
		data.append("photo", {
			name: photo.uri.substring(45, 55) + ".jpg",
			type: "image/jpg",
			uri:
				Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
		})

		Object.keys(body).forEach(key => {
			data.append(key, body[key])
		})

		return data
	}
}
class CustomCamera extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			camera: "",
			photo: "",
			actIn: false,
			imagePath: null,
			latitude: null,
			longitude: null
		}
	}

	render() {
		// this.findCurrentLocation()
		const { container } = navBarStyles
		return (
			<View style={container}>
				<Navbar title="PLANET-P" />
				<View style={container}>
					{this.state.imagePath ? this.renderImage() : this.renderCamera()}
				</View>
				<ButtomBar />
			</View>
		)
	}

	renderCamera() {
		const {
			container,
			preview,
			cameraSurface,
			capture,
			cameraMenuFont
		} = cameraStyles
		if (this.props.navigation.isFocused()) {
			return (
				<View style={container}>
					<RNCamera
						style={preview}
						type={RNCamera.Constants.Type.back}
						flashMode={RNCamera.Constants.FlashMode.auto}
						permissionDialogTitle={"Permission to use camera"}
						permissionDialogMessage={
							"We need your permission to use your camera phone"
						}
					>
						{({ camera, status }) => {
							if (status !== "READY") return <PendingView />
							return (
								<View style={cameraSurface}>
									<TouchableOpacity
										onPress={() => this.takePicture(camera)}
										style={capture}
									>
										<Text style={cameraMenuFont}>Capture</Text>
									</TouchableOpacity>
									<TouchableOpacity
										style={capture}
										onPress={this.handleChoosePhoto}
									>
										<Text style={cameraMenuFont}>Choose Photo</Text>
									</TouchableOpacity>
								</View>
							)
						}}
					</RNCamera>
				</View>
			)
		}
		return null
	}

	takePicture = camera => {
		const options = {
			quality: 0.5,
			base64: true,
			base64: false,
			doNotSave: false,
			skipProcessing: true
		}
		camera.takePictureAsync(options).then(data => {
			camera.pausePreview()
			this.setState({ imagePath: data.uri })
			this.setState({ photo: data })
			console.log("THIS IS DATAAAAAA", data)
		})
	}

	handleChoosePhoto = () => {
		const options = {
			noData: true
		}
		ImagePicker.launchImageLibrary(options, response => {
			if (response.uri) {
				this.setState({ imagePath: response.uri })
				this.setState({ photo: response })
				console.log("RESPONSEEEEEEEE", response)
			}
		})
	}

	handleUploadPhoto = async () => {
		// this.actTrig()
		await uploadModule(
			`${ipaddr}:${port}/imgs`,
			createFormData(this.state.photo, {
				latitude: this.state.latitude,
				longitude: this.state.longitude
			})
		)
			.then(response => response.json())
			.then(response => {
				console.log("upload success", response)
				this.setState({ imagePath: null })
				this.setState({ photo: null })
				// this.setState({ actIn: false })
				ToastAndroid.show("Upload successfully!", ToastAndroid.SHORT)
			})
			.catch(error => {
				console.log("upload error", error)
				ToastAndroid.show("Upload failed!", ToastAndroid.SHORT)
				// alert("Upload failed!")
			})
	}

	renderImage() {
		const {
			container,
			imgPreview,
			capture,
			cameraMenuFont,
			cameraMenu,
			loading
		} = cameraStyles
		return (
			<View style={container}>
				{/* {this.state.actIn ? (
					<View style={loading}>
						<ActivityIndicator
							justifyContent="center"
							size="large"
							color="#00ff00"
						/>
					</View>
				) : null} */}
				<Image source={{ uri: this.state.imagePath }} style={imgPreview} />
				<View style={cameraMenu}>
					<TouchableOpacity style={capture} onPress={this.handleUploadPhoto}>
						<Text style={cameraMenuFont}>OK</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={capture}
						onPress={() => this.setState({ imagePath: null })}
					>
						<Text style={cameraMenuFont}>Cancel</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	savePicture = function(imgPath) {
		console.log("THIS IS THE PATHHHHH", imgPath)

		CameraRoll.saveToCameraRoll(imgPath, "photo")
			.then(async () => {
				await this.getGeolocation(imgPath)
				await this.setState({ imagePath: null })
				ToastAndroid.show("Save image sucessfully", ToastAndroid.SHORT)
			})
			.catch(error => console.error(error))
	}

	getGeolocation = imgPath => {
		navigator.geolocation.getCurrentPosition(
			position => {
				let mutatePosition = { ...position }
				mutatePosition["uri"] = imgPath
				mutatePosition["name"] = `NSTDA-Cassava field#${randomInteger(50)}`
				SaveLocationFile(mutatePosition)
					.then(() => console.log("Save file complete"))
					.catch(saveLocationError =>
						console.log("Save location error:", saveLocationError.code)
					)
			},
			error => console.log("Error on getGeolocation():", error),
			{
				timeout: LOCATION_REQUEST_TIMEOUT,
				maximumAge: LOCATION_REQUEST_MAX_AGE,
				enableHighAccuracy: false
			}
		)
	}

	findCurrentLocation = () => {
		navigator.geolocation.getCurrentPosition(
			position => {
				console.log("Position:", position)
				const latitude = JSON.stringify(position.coords.latitude)
				const longitude = JSON.stringify(position.coords.longitude)
				this.setState({
					latitude: latitude,
					longitude: longitude
				})
				console.log("THIS IS LAT ", this.state.latitude)
				console.log("THIS IS LONG ", this.state.longitude)
			},
			error => console.log(error),
			{
				timeout: 30000,
				maximumAge: 10000,
				enableHighAccuracy: false
			}
		)
	}

	actTrig = () => {
		this.setState({ actIn: true })
	}

	componentDidMount = async () => {
		try {
			this.findCurrentLocation()
			console.log("lat long from DidMount")
		} catch (didMountError) {
			console.log("DidMountError => ", JSON.stringify(didMountError))
		}
	}
}

export default withNavigationFocus(CustomCamera)
