import React, { Component } from "react"
import logo from "../../assets/planet_p_logo.png"
import LinearGradient from "react-native-linear-gradient"
import {
	View,
	Text,
	TextInput,
	Image,
	TouchableOpacity,
	ToastAndroid,
	ScrollView
} from "react-native"
import { Actions } from "react-native-router-flux"
import { logoPageStyle } from "../styles/logoPageStyles"
import Toast from "react-native-tiny-toast"
import validator from "validator"
import { ipaddr, port } from "../configs/configuration"
///////////////////////////////////////////////////////
const passwordValidator = require("password-validator")
const schema = new passwordValidator()
const mobileschema = new passwordValidator()

schema
	.is()
	.min(3) // Minimum length 3
	.is()
	.max(25) // Maximum length 25
	.has()
	.uppercase() // Must have uppercase letters
	.has()
	.lowercase() // Must have lowercase letters
	.has()
	.digits() // Must have digits
	.has()
	.not()
	.spaces() // Should not have spaces
	.is()
	.not()
	.oneOf(["Passw0rd", "Password123"]) // Blacklist these values

mobileschema
	.is()
	.min(10)
	.is()
	.max(10)
///////////////////////////////////////////////////////

let seen = []
class SignUp extends Component {
	constructor(props) {
		super(props)
		this.state = {
			name: "",
			email: "",
			password: "",
			cfpass: "",
			mobile: ""
		}
	}

	postSignUp = () => {
		//To sign up user
		const { name, email, cfpass, password, mobile } = { ...this.state }
		console.log("pass", password)
		// console.log("cfpass", cfpass)
		console.log("maillll", email)
		if (name && email && password && cfpass && mobile != "") {
			if (validator.isEmail(this.state.email) === true) {
				// Check if the email is in a good format
				if (password != cfpass) {
					// alert("your password is not matched")
					ToastAndroid.show("your password is not matched", ToastAndroid.SHORT)
				} else {
					if (schema.validate(this.state.password) === true) {
						if (mobileschema.validate(this.state.mobile) === true) {
							// Check if the password is in a good format
							fetch(`${ipaddr}:${port}/users`, {
								// fetch data to backend
								method: "POST",
								headers: {
									Accept: "application/json",
									"Content-Type": "application/json"
								},
								body: JSON.stringify(
									{
										name: name,
										email: email,
										password: password,
										mobile: mobile
									},
									function(key, val) {
										if (val != null && typeof val == "object") {
											if (seen.indexOf(val) >= 0) {
												return
											}
											seen.push(val)
										}
										return val
									}
								)
							}).then(responseData => {
								console.log("response at front", responseData.status)
								console.log("responseData at front", responseData)
								if (responseData.status === 201) {
									// if the email is not already in used
									this.setState({
										name: null,
										email: null,
										password: null,
										mobile: null
									})
									Toast.showSuccess("Signed up sucessfully", { duration: 800 })
									Actions.LogoPage()
								} else {
									ToastAndroid.show(
										"this e-mail is already used",
										ToastAndroid.SHORT
									)
								}
							})
						} else {
							ToastAndroid.show(
								"Bad mobile format , try again",
								ToastAndroid.SHORT
							)
						}
					} else {
						// ToastAndroid.show(
						// 	"Bad password format , try again",
						// 	ToastAndroid.SHORT
						// )
						Toast.show(
							"password min length is 3 , must consist of 1 uppercase,lowercase,number",
							{ duration: 4500 }
						)
					}
				}
			} else {
				// alert("Bad e-mail format")
				ToastAndroid.show("Bad email format , try again", ToastAndroid.SHORT)
			}
		} else {
			ToastAndroid.show("Please fill all columns", ToastAndroid.SHORT)
		}
	}
	render() {
		const {
			container,
			imageSignupStyle,
			textInputContainer,
			loginSignupButton,
			loginButtonFont,
			menuFloat
		} = logoPageStyle
		return (
			<LinearGradient colors={["#15B8C4", "#18B691"]} style={container}>
				<ScrollView style={menuFloat}>
					<Image style={imageSignupStyle} source={logo} />
					<View style={{ marginTop: 0 }}>
						<TextInput
							style={textInputContainer}
							placeholder="Enter your name"
							autoCapitalize="none"
							onChangeText={name => this.setState({ name })}
							value={this.state.name}
						/>
						<TextInput
							style={textInputContainer}
							autoCapitalize="none"
							placeholder="Email Address"
							onChangeText={email => this.setState({ email })}
							value={this.state.email}
							keyboardType={"email-address"}
						/>
						<TextInput
							style={textInputContainer}
							autoCapitalize="none"
							secureTextEntry={true}
							placeholder="Password"
							onChangeText={password => this.setState({ password })}
							value={this.state.password}
						/>
						<TextInput
							style={textInputContainer}
							secureTextEntry={true}
							autoCapitalize="none"
							placeholder="Comfirm Password"
							onChangeText={cfpass => this.setState({ cfpass })}
							value={this.state.cfpass}
						/>
						<TextInput
							style={textInputContainer}
							placeholder="Mobile"
							onChangeText={mobile => this.setState({ mobile })}
							value={this.state.mobile}
							keyboardType={"phone-pad"}
						/>
						<TouchableOpacity
							style={loginSignupButton}
							onPress={this.postSignUp}
						>
							<Text style={loginButtonFont}>Sign Up</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</LinearGradient>
		)
	}
}

export default SignUp
