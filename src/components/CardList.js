import React, { Component } from "react"
import CardDetail from "./CardDetail"
import RNFS from "react-native-fs"
import {
	View,
	ScrollView,
	Text,
	CameraRoll,
	TouchableOpacity,
	ActivityIndicator
} from "react-native"
import { withNavigationFocus } from "react-navigation"
import AsyncStorage from "@react-native-community/async-storage"
import { connect } from "react-redux"
import { showInfo } from "../store/actions/userinfo"
import { Navbar, ButtomBar } from "./common"
import { navBarStyles } from "../styles/navBarStyles"
import { ipaddr, port } from "../configs/configuration"

class CardList extends Component {
	constructor(props) {
		super(props)
		this.state = {
			images: [],
			locations: [],
			data: []
		}
	}

	render() {
		const { container, actIn } = navBarStyles
		if (this.state.data.length > 0) {
			return (
				<View style={container}>
					<Navbar title="PLANET-P" />
					<ScrollView>
						{this.state.data.map(card => {
							return (
								<CardDetail
									key={card.imgid}
									CardData={card.url}
									createdDate={card.curtime.substring(0, 10)}
									createdTime={card.curtime.substring(11, 19)}
									locationData={{ lat: card.lat, long: card.long }}
								/>
							)
						})}
					</ScrollView>
					<ButtomBar />
				</View>
			)
		}
		return (
			<View style={[actIn]}>
				<Navbar title="PLANET-P" />
				<ActivityIndicator
					justifyContent="center"
					size="large"
					color="#15B8C4"
				/>
				<ButtomBar />
			</View>
		)
	}

	renderCardList = async () => {
		const TokenInStorage = await AsyncStorage.getItem("token")
		await fetch(`${ipaddr}:${port}/cardlist`, {
			method: "GET",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				authorization: TokenInStorage
			}
		})
			.then(response => {
				if (response.status === 200) return response.json()
			})
			.then(response => {
				console.log("responseData => ", response)
				this.setState({
					data: response
				})
				console.log("data in state laew krubbb=> ", this.state.data)
			})
			.catch(error => {
				console.error("errorrrrrrrrrrr", error)
				throw error
			})
	}

	componentDidMount = async () => {
		try {
			this.readImagesFromFile()
			this.readLocationsFromFile()
			this.renderCardList()
			const JWT = await AsyncStorage.getItem("token")
			this.props.showInfo(JWT)
			console.log("DidMount => ", this.props.data)
		} catch (didMountError) {
			console.log("DidMountError => ", JSON.stringify(didMountError))
		}
	}

	readLocationsFromFile = () => {
		const appDocumentDirectoryPath = `${
			RNFS.DocumentDirectoryPath
		}/locationInfo.txt`
		RNFS.exists(appDocumentDirectoryPath)
			.then(found => {
				if (found) {
					RNFS.readFile(appDocumentDirectoryPath, "utf8").then(readData => {
						const locations = JSON.parse(
							`[${readData.substr(0, readData.length - 1)}]`
						)
						this.setState({ locations })
					})
				}
			})
			.catch(existsError => {
				console.log("Error in readLocationsFromFile:", existsError)
			})
	}

	readImagesFromFile = () => {
		CameraRoll.getPhotos({
			first: 20,
			assetType: "Photos",
			groupName: "DCIM"
		})
			.then(resultImages => {
				this.setState({ images: resultImages.edges })
			})
			.catch(error => {
				console.log("eueu", error)
			})
	}
}

const mapStateToProps = state => {
	return {
		data: state.UserInfoReducer.data //initial state in reducer
	}
}

const mapDispatchToProps = { showInfo } //action

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CardList)
