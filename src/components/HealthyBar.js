import React from "react";
import { Text, View } from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import { healthyBarStyle } from "../styles/healthyBarStyles";
import { CardSection } from "./common";
import { randomInteger } from "../utils/randomInteger";

const MAX_WIDTH = 175;
const healthyBar = () => {
    return (
        <CardSection>
            <Text>Healthy level: Bad </Text>
                <LinearGradient colors={["#FF0000", "#00FF00"]} 
                    start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                    style={healthyBarStyle(25).indicator}>
                    <View style={healthyBarStyle(25).nullTube}>
                    </View>
                </LinearGradient>
            <Text> Good</Text>
        </CardSection>
    )
}

export default healthyBar;