import React from "react";
import { Text } from "react-native";
import { CardSection } from "./common";
import { randomInteger } from "../utils/randomInteger";

const DISEASES = ["Black spot"];

const diseaseInfo = () => {
    const randDisease = DISEASES[randomInteger(DISEASES.length)];
    return (
        <CardSection>
            <Text>
                {`Disease: ${randDisease}`}
            </Text>
        </CardSection>
    )
}

export default diseaseInfo;