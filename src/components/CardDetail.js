import React from "react"
import { Text, View, Image } from "react-native"

import { CardSection, Card, Button } from "./common"
import { cardDetailStyles } from "../styles/cardDetailStyles"
import { Actions } from "react-native-router-flux"
// const CardDetail = ({ CardData, locationData }) => {
const CardDetail = props => {
	const imageUri = props.CardData
	const { lat, long } = props.locationData
	const createdDate = props.createdDate
	const createdTime = props.createdTime

	// const { image } = CardData.node
	const { headerContentStyle, imageStyle, textStyle } = cardDetailStyles

	return (
		<Card>
			<CardSection>
				<View style={headerContentStyle}>
					<Text
						style={textStyle}
					>{`Created on: ${createdDate}   Time: ${createdTime}`}</Text>
					{locationComponent(lat, long)}
				</View>
			</CardSection>
			<CardSection>
				<Image style={imageStyle} source={{ uri: imageUri }} />
			</CardSection>
			<CardSection>
				<Button
					text="More Detail"
					onPress={() => {
						Actions.Detail({ lat, long, createdDate, createdTime, imageUri })
					}}
				/>
			</CardSection>
		</Card>
	)
}

const locationComponent = (lat, long) => {
	const { textStyle } = cardDetailStyles
	if (lat && long)
		return <Text style={textStyle}>{`Location: ${lat}, ${long}`}</Text>
	return <Text style={textStyle}>{`Location: User does not defined`}</Text>
}

export default CardDetail
