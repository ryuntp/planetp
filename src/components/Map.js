import React, { Component } from "react"
import _ from "lodash"
import MapView, { PROVIDER_GOOGLE } from "react-native-maps"
import RNFS from "react-native-fs"
import { View, StyleSheet } from "react-native"
import AsyncStorage from "@react-native-community/async-storage"
import { withNavigationFocus } from "react-navigation"
import { Navbar, ButtomBar } from "./common"
import { navBarStyles } from "../styles/navBarStyles"
import { startRegion } from "../utils/constants"
import MapMarker from "./common/MapMarker"
import { ipaddr, port } from "../configs/configuration"

class Map extends Component {
	constructor(props) {
		super(props)
		this.state = {
			locations: [],
			images: [],
			data: []
		}
	}

	renderPins() {
		return this.state.data.map(card => {
			return (
				<MapMarker
					key={card.imgid}
					latitude={card.lat}
					longitude={card.long}
					// title={location.name}
					uri={card.url}
				/>
			)
		})
	}

	componentDidMount() {
		this.readLocationsFromFile()
		this.renderCardList()
		console.log("SAWASDEEEE", this.state.data)
	}

	readLocationsFromFile = () => {
		const appDocumentDirectoryPath = `${
			RNFS.DocumentDirectoryPath
		}/locationInfo.txt`
		RNFS.exists(appDocumentDirectoryPath)
			.then(found => {
				if (found) {
					RNFS.readFile(appDocumentDirectoryPath, "utf8").then(readData => {
						const locations = JSON.parse(
							`[${readData.substr(0, readData.length - 1)}]`
						)
						this.setState({ locations })
					})
				}
			})
			.catch(existsError => {
				console.log("Error in readLocationsFromFile:", existsError)
			})
	}

	renderCardList = async () => {
		const TokenInStorage = await AsyncStorage.getItem("token")
		await fetch(`${ipaddr}:${port}/cardlist`, {
			method: "GET",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				authorization: TokenInStorage
			}
		})
			.then(response => {
				if (response.status === 200) return response.json()
			})
			.then(response => {
				console.log("responseData => ", response)
				this.setState({
					data: response
				})
				console.log("data in state laew krubbb=> ", this.state.data)
			})
			.catch(error => {
				console.error("errorrrrrrrrrrr", error)
				throw error
			})
	}
	render() {
		const { container } = navBarStyles
		if (this.props.navigation.isFocused()) {
			return (
				<View style={container}>
					<Navbar title="PLANET-P" />
					<View style={container}>
						<MapView
							provider={PROVIDER_GOOGLE}
							initialRegion={startRegion}
							style={{ ...StyleSheet.absoluteFillObject }}
						>
							{this.renderPins()}
						</MapView>
					</View>
					<ButtomBar />
				</View>
			)
		} else {
			return null
		}
	}
}

export default withNavigationFocus(Map)
