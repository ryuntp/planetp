import { SHOW_INFO } from "./actionTypes"
import {
	ipaddr,
	port
} from "/home/biosci/Desktop/planet-p/src/configs/configuration"

export const showInfo = JWT => {
	return async dispatch => {
		console.log("JWT weiiii => ", JWT)
		try {
			const response = await fetch(`${ipaddr}:${port}/userinfo`, {
				method: "GET",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
					authorization: JWT
				}
			})
			const rawData = await response.json()
			console.log("RESPONSE => ", rawData)
			dispatch({ type: SHOW_INFO, payload: rawData })
		} catch (requestError) {
			console.log("Error in action => ", JSON.stringify(requestError))
			throw requestError
		}
	}
}
