import UserInfoReducer from "./userInfo"
import { combineReducers } from "redux"

const rootReducer = combineReducers({
	UserInfoReducer
})

export default rootReducer
