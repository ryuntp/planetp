import { SHOW_INFO } from "../actions/actionTypes"
const initialState = {
	data: ""
}
const UserInfoReducer = (state = initialState, action) => {
	switch (action.type) {
		case SHOW_INFO:
			return {
				...state,
				data: action.payload
			}

		default:
			return state
	}
}

export default UserInfoReducer
