import RNFS from "react-native-fs";

const saveLocationToFile = (location) => {
    const writeFilePath = RNFS.DocumentDirectoryPath + "/locationInfo.txt";
    return new Promise((resolve, reject) => {
        RNFS.exists(writeFilePath)
            .then((found) => {
                if(found) {
                    RNFS.readFile(writeFilePath, "utf8")
                        .then(readContent => {
                            const writeContent = readContent + JSON.stringify(location) + ",";
                            RNFS.writeFile(writeFilePath, writeContent, "utf8")
                                .then(() => resolve("Write complete"))
                        })
                        .catch(writeError => reject(writeError));
                } else {
                    console.log("Does not exist!");
                    const firstWriteFile = JSON.stringify(location) + ",";
                    RNFS.writeFile(writeFilePath, firstWriteFile, "utf8")
                        .then(() => resolve("Write complete"))
                        .catch(writeError => reject(writeError));
                }
            })
            .catch((err) => reject(err));
    });
}

export default saveLocationToFile;