export const startRegion = {
    latitude: 14.0805834,
    longitude: 100.5940108,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421
};

export const MONTHS = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
]

export const LOCATION_REQUEST_TIMEOUT = 20000;
export const LOCATION_REQUEST_MAX_AGE = 1000;