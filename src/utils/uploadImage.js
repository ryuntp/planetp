import AsyncStorage from "@react-native-community/async-storage"

export const uploadModule = async (url, bodyObj) => {
	const TokenInStorage = await AsyncStorage.getItem("token")
	return fetch(url, {
		method: "POST",
		headers: { authorization: TokenInStorage },
		body: bodyObj
	})
}
