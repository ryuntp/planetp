import React from "react";

test("1 equal to 1", () => expect(1).toEqual(1));

const constants = {
    Aspect: {},
    BarCodeType: {},
    Type: {},
    CaptureMode: {},
    CaptureTarget: {},
    CaptureQuality: {},
    Orientation: {},
    FlashMode: {},
    TorchMode: {}
};

class Camera extends React.Component {
    static constants = constants;
    render() {
        return null;
    }
}

Camera.constants = constants;
export default Camera;

