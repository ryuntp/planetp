import React from "react";
import LogoPage from "../src/components/Logopage";

import renderer from "react-test-renderer";

test("Logo page renders correctly", () => {
    const logoPage = renderer.create(<LogoPage />).toJSON();
    expect(logoPage).toMatchSnapshot();
});
