import React from "react";
import renderer from "react-test-renderer";
import Camera from "../src/components/Camera";
import * as mockCamera from "./__mocks__/react-native-camera";
import * as mockRNFS from "./__mocks__/react-native-fs";

jest.mock("react-native-camera", () => mockCamera);
jest.mock("react-native-fs", () => mockRNFS);

test("Logo page renders correctly", () => {
    const camera = renderer.create(<Camera />).toJSON();
    expect(camera).toMatchSnapshot();
});
